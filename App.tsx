import * as React from 'react';
import {
  SafeAreaView,
} from 'react-native';

// import DumpComponent from './src/DumpComponent';
import StatefullComponent from './src/StatetfullComponent';

const App = () => {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      {/* <DumpComponent title={"You know nothing"} subtitle={"John Snow"} /> */}
      <StatefullComponent />
    </SafeAreaView>
  );
};

export default App;
