import { useCallback, useState } from 'react';

export default (initialState = 0) => {
    const [counter, setCounter] = useState<number>(initialState);

    const increment = useCallback(() => { setCounter((currentState) => currentState + 1) }, []);
    const decrement = useCallback(() => { setCounter((currentState) => currentState > 0 ? currentState - 1 : 0) }, []);

    return { counter, increment, decrement };
}

