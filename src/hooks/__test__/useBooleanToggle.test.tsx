import * as React from 'react';
import { shallow } from "enzyme";
import { act } from "react-dom/test-utils";
import useBooleanToggle from '../useBooleanToggle';

export const mountReactHook = (hook: any) => {
    const componentHook = {};
    let componentMount;
    const Component = ({ children }: any) => children(hook());

    act(() => {
        componentMount = shallow(
            <Component>
                {(hookValues: any) => {
                    Object.assign(componentHook, hookValues);
                    return null;
                }}
            </Component>
        );
    });
    return { componentMount, componentHook };
};

describe('useBooleanToggle', () => {
    let hook: { state: boolean; toggle: () => void; };
    let setupComponent: any;

    beforeEach(() => {
        setupComponent = mountReactHook(useBooleanToggle);
        hook = setupComponent.componentHook;
    })


    it('should have initial value false', async () => {
        expect(hook.state).toBeFalsy();
    });

    test('the toggle should toggle XD', async () => {
        act(() => {
            hook.toggle()
        })
        expect(hook.state).toBeTruthy()
    })
});