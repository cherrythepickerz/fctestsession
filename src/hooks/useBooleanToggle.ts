import { useCallback, useState } from 'react';

export default (initialState = false) => {
    const [state, setState] = useState<boolean>(initialState);

    const toggle = useCallback(() => { setState((currentState) => !currentState) }, []);

    return { state, toggle };
}

