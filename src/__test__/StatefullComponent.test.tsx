import { mount, ReactWrapper } from 'enzyme';
import * as React from 'react';
import { act } from 'react-dom/test-utils';
// import { act } from 'react-test-renderer';
import StatefullComponent, { Analytics } from '../StatetfullComponent';

describe('StatefullComponent', () => {
    let wrapper: ReactWrapper<any, Readonly<{}>, React.Component<{}, {}, any>>;

    beforeEach(() => {
        wrapper = mount(<StatefullComponent />);
    })

    describe('Basic tests', () => {
        it('should renders correctly', () => {
            expect(wrapper).toMatchSnapshot()
        })
    })

    describe('Dogs counter', () => {
        describe('On increment counter', () => {
            let analyticsSpy: any;
            beforeEach(() => {
                jest.clearAllMocks()
                analyticsSpy = jest.spyOn(Analytics, 'analyzeDogCounter');
                const incrementBtn = wrapper.find('[testID="statefullComponentIncrementBtn"]')
                act(() => {
                    incrementBtn.first().simulate('click');
                    wrapper.update()
                })
            })

            test('The analytics should be triggered', () => {
                expect(analyticsSpy).toHaveBeenCalled();
            });

            test('The dogs counter should increment', () => {
                const counter = wrapper.find("[testID='statefullComponentDogCounter']")
                const counterText = counter.first().text();

                expect(counterText).toEqual(`How many dogs enough? 1`)
            })
        })

        describe('On decrement counter', () => {
            let analyticsSpy: any;
            beforeEach(async () => {
                jest.clearAllMocks()
                analyticsSpy = jest.spyOn(Analytics, 'analyzeDogCounter');
                const incrementBtn = wrapper.find('[testID="statefullComponentIncrementBtn"]')
                const decrementBtn = wrapper.find('[testID="statefullComponentDecrementBtn"]')
                await act(async () => {
                    await incrementBtn.first().simulate('click');
                    await incrementBtn.first().simulate('click');
                    await incrementBtn.first().simulate('click');
                    await incrementBtn.first().simulate('click');
                    await decrementBtn.first().simulate('click');
                })
                wrapper.update()
            })

            test('The analytics should be triggered as many as btn clicked', () => {
                expect(analyticsSpy).toHaveBeenCalledTimes(5);
            });
            test('The dogs counter should decrement', () => {
                const counter = wrapper.find("[testID='statefullComponentDogCounter']")
                const counterText = counter.first().text();

                expect(counterText).toEqual(`How many dogs enough? 3`)
            })
        })
    })

    describe('on let dogs out btn press', () => {
        xtest('analytics should be triggered', () => { });
        xtest('the images should appears on the screen', () => { })
    })

})