import * as React from 'react';
import DumpComponent from "../DumpComponent"
import { shallow, ShallowWrapper } from 'enzyme';

const dumpComponentPropsMock = {
    title: "The beggining is the end",
    subtitle: "And the end is the beggining"
};

describe("Dump component", () => {
    describe('Basic tests', () => {
        let wrapper: ShallowWrapper<typeof dumpComponentPropsMock, Readonly<{}>, React.Component<typeof dumpComponentPropsMock, {}, typeof DumpComponent>>;

        beforeEach(() => {
            wrapper = shallow(<DumpComponent {...dumpComponentPropsMock} />);
        });
        it('renders correctly', () => {
            expect(wrapper).toMatchSnapshot();
        });

        it('renders title and subtitle', () => {
            const title = wrapper.find('[testID="dumpComponentTitle"]').childAt(0).text();
            const subtitle = wrapper.find('[testID="dumpComponentSubtitle"]').childAt(0).text();

            expect(title).toBe(dumpComponentPropsMock.title)
            expect(subtitle).toBe(dumpComponentPropsMock.subtitle)
        })
    })

})