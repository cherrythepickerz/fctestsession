import * as React from 'react';
import { View, Text } from 'react-native';

type Props = {
    title: string,
    subtitle?: string,
}

const DumpComponent = ({ title, subtitle }: Props) => {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text testID={'dumpComponentTitle'}>{title}</Text>
            <Text testID={'dumpComponentSubtitle'}>{subtitle}</Text>
        </View>
    )
}

export default DumpComponent;