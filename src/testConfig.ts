import { configure } from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';

/**
    Workaround to eliminate annoying warnings while running tests
    this should be resolved once a proper RN adapter is available
    https://github.com/enzymejs/enzyme/issues/1436
*/

const ALLOWED_ERROR_TAGS = ['failed prop type']
//@ts-ignore
console.error = e => {
    for (const tag of ALLOWED_ERROR_TAGS) {
        if (e.toLowerCase().includes(tag.toLowerCase())) {
            console.warn(e);
            break
        }
    }
};

configure({ adapter: new Adapter() });
