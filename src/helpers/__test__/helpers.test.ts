import { summ } from "../index"

describe('helpers', () => {
    describe('summ', () => {
        it('should return 4 when passing 2 and 2', () => {
            expect(summ(2, 2)).toBe(4)
        })
    })
})