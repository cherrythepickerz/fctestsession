import * as React from 'react';
import { View, Image, Text, TouchableOpacity } from 'react-native';
//@ts-ignore
import dogImage from './images/dogs.png';

const getRandomStyle = () => `${Math.random() * 100}%`;

export const Analytics = (() => ({
    loadPage: () => { console.log('page loaded') },
    analyzeDogCounter: (counter: number) => { console.log(`Dogs analysed: ${counter}`) },
}))()

const StatefullComponent = () => {
    const [areDogsVisible, setDogsVisible] = React.useState<boolean>(false);
    const [dogCounter, setDogCounter] = React.useState<number>(0);

    const incrementDog = () => {
        console.log('increment dog')
        setDogCounter(dogCounter + 1);
    }

    const decrementDog = () => {
        if (dogCounter === 0) { return }
        setDogCounter(dogCounter - 1)
    }

    const onBtnPress = () => {
        setDogsVisible(!areDogsVisible);
    }

    const renderDogs = () => {
        let counter = dogCounter;
        const dogs = []

        while (counter > 0) {
            dogs.push(<Image key={counter} testID={`statefullComponentImage-${counter}`} resizeMode={'contain'} style={{ height: 100, width: 100, left: getRandomStyle(), top: getRandomStyle() }} source={dogImage} />)
            counter--
        }

        return dogs
    }

    React.useEffect(() => {
        Analytics.loadPage();
    }, []);

    React.useEffect(() => {
        console.log('useEffect')
        Analytics.analyzeDogCounter(dogCounter);
    }, [dogCounter]);

    return (
        <View style={{ alignItems: 'center', justifyContent: 'space-around', flex: 1 }}>
            <View>
                <Text testID={'statefullComponentDogCounter'} style={{ fontSize: 24, marginVertical: 16 }}>{`How many dogs enough? ${dogCounter}`}</Text>
                <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                    <TouchableOpacity testID={'statefullComponentDecrementBtn'} onPress={decrementDog}>
                        <Text>LESS</Text>
                    </TouchableOpacity>
                    <TouchableOpacity testID={'statefullComponentIncrementBtn'} onPress={incrementDog}>
                        <Text>MORE</Text>
                    </TouchableOpacity>
                </View>
            </View>

            <View style={{ alignItems: 'center' }}>
                <Text>Let the dogs out?</Text>
                <TouchableOpacity testID={'statefullComponentBtn'} style={{ backgroundColor: '#f06292', padding: 20, borderRadius: 4, marginTop: 8 }} onPress={onBtnPress}>
                    <Text>OH YES</Text>
                </TouchableOpacity>
            </View>
            {
                areDogsVisible ?
                    <>
                        <Text testID={'statefullComponentSubtitle'}>Woof, woof, woof, woof, woof!</Text>
                        <View style={{ position: 'absolute' }} pointerEvents={'none'}>
                            {renderDogs()}
                        </View>
                    </>
                    : undefined
            }
        </View>
    )
}

export default StatefullComponent;